package com.agilelab.bigdata.workload.spark.app

import org.apache.spark.sql.SparkSession

object SparkApp {

  def main(args: Array[String]): Unit = {
    implicit val spark = SparkSession.builder().appName("${{ values.name }}").getOrCreate()
    import spark.implicits._

    Ingestion.start()

    spark.stop()
  }
}
