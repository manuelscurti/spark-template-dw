package com.agilelab.bigdata.workload.spark.app

import org.apache.spark.sql.SparkSession
import com.typesafe.config.Config

object Ingestion {

  val config = ConfigFactory.load().getConf("${{ values.name }}")

  def start()(implicit session: SparkSession): Unit = {
    val inputPath = config.getString("input-path")
    val outputTable = config.getString("output-table")

    spark
      .read
      .parquet(inputPath)
      .createOrReplaceTempView("${{ values.tableName }}")

    val df = spark.sql(config.getString("transform-query")).persist()

    logger.info("Table row count: {}", df.count)

    df.write.options("mode", "overwrite").saveAsTable(outputTable)
  }


}