# Spark template

This template can be used to create a Spark template on the CDP-AWS infrastructure.
In order to customize the template:
* Update the *spec.mesh* section inside *catalog-info.yaml*, according to the [Open dataproduct specification](https://github.com/agile-lab-dev/Data-Product-Specification) and to the [Spark specific provisioner contract](https://gitlab.com/AgileFactory/Witboost.Mesh/Provisioning/cdp/witboost.mesh.provisioning.workload.cdp.spark);
* if you need more artifacts (like Spark job dependencies) upload them on s3 through the cicd and reference them inside the *catalog-info.yaml* file